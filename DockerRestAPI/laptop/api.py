# Laptop Service

import flask, sys
from flask import Flask, request, render_template, redirect, url_for, flash, Markup
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Resource, Api

import csv
import os
from pymongo import MongoClient
import logging

# Instantiate the app
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():

    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance',type=str)
    brevet_start_date = request.args.get('begin_date', type=str)
    brevet_start_time = request.args.get('begin_time', type=str)
    brevet_start = brevet_start_date + " " + brevet_start_time

    if km > int(dist[:-2])*1.2:
        result = {"open": 0, "close": 0}
        return flask.jsonify(result = result)

    if km > int(dist[:-2]) and km <= int(dist[:-2])*1.2:
        km = int(dist[:-2])
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, int(dist[:-2]), brevet_start)
    close_time = acp_times.close_time(km, int(dist[:-2]), brevet_start)

    if km ==int(dist[:-2]):
        close = arrow.get(close_time)
        if km == 200:
            close = close.shift(minutes =+ 10)
            close_time = close.isoformat()
        elif km == 400:
            close = close.shift(minutes =+ 20)
            close_time = close.isoformat()

    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


#############
open_confirmed = []
close_confirmed = []
km_confirmed = []

_times = db.tododb.find()
times = [item for item in _times]

@app.route('/display', methods=["POST"])
def todo():
    global db, client, open_confirmed, close_confirmed, km_confirmed, _times, times

    _items = db.tododb.find()
    items = [item for item in _items]
    length = len(items)
    
    if length == 0:
        flash("Database is Empty, Please Submit Form with Values. Database Clears After Invocation of 'Display'.")
        return redirect(url_for("index"))

    db.tododb.drop()
    db = client.tododb

    _times = db.tododb.find()
    times = [item for item in _times]
    
    open_confirmed = []
    close_confirmed = []
    km_confirmed = []
    
    flash("Display Button Was Clicked")
    return flask.render_template('db_display.html', items=items)


@app.route('/new', methods = ["POST"])
def new():


    global open_confirmed, close_confirmed, km_confirmed, _times, times
    
    open_time = request.form.getlist("open")
    close_time = request.form.getlist("close")
    controls = request.form.getlist("km")

    open_times = []
    close_times = []
    km = []

    for i in open_time:
        i = str(i)
        if (i != "") and (i not in open_confirmed):
            open_times.append(i)

    for j in close_time:
        j = str(j)
        if (j != "") and (j not in close_confirmed):
            close_times.append(j)

    for l in controls:
        l = str(l)
        if (l != "") and (l not in km_confirmed):
            km.append(l)

    length = len(open_times)

    if len(open_times) == 0 or len(close_times) == 0:
        flash("No New Controls Were Given")
        return redirect(url_for("index"))
    
        
    for k in range(length):

        open_confirmed.append(open_times[k])
        close_confirmed.append(close_times[k])
        km_confirmed.append(km[k])

        times = {"open_time": open_times[k], "close_time": close_times[k], "km": km[k]}

        db.tododb.insert_one(times)

    flash("Submit Button Was Clicked!")
    
    _times = db.tododb.find()
    times = [item for item in _times]
            
    return redirect(url_for("index"))


class listAll1(Resource):
    
    def get(self):
        global times
        
        vals = {}
        for i in times:
            vals[i["km"]] = ["open time: " + i["open_time"], "close time: " + i["close_time"]]
                    
        return vals

        
class listOpenOnly1(Resource):

    def get(self):
        global times
        
        vals = {}
        for i in times:
            vals[i["km"]] = i["open_time"]
                
        return vals

class listCloseOnly1(Resource):
    def get(self):
        global times
        
        vals = {}
        for i in times:
            vals[i["km"]] = i["close_time"]
                
        return vals

class listAll2(Resource):
    
    def get(self, spec1):
        global times

        top = request.args.get("top")
        
        if spec1 == "csv":
            
            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]

            string = ""
            '''
            with open("listall.csv",'w') as csvFile:
                writer = csv.writer(csvFile, delimiter = ",", quotechar = "|", quoting = csv.QUOTE_MINIMAL)
                for i in temp_array2:
                    writer.writerow([i["km"], i["open_time"], i["close_time"]])

            This code doesn't work, but it should. A CSV file is created, but it cannot be displayed to the page. 
            '''
            for i in temp_array2:
                
                string += "km:" + i["km"] + ", " + "open time:" + i["open_time"] + ", " + "close time:" + i["close_time"] + ", "
                    
            return string

        elif spec1 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km'])) 
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = ["open time: " + i["open_time"], "close time: " + i["close_time"]]
                    
            return vals
            

        
class listOpenOnly2(Resource):

    def get(self, spec2):
        global times

        top = request.args.get("top")
        
        if spec2 == "csv":

            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km'])) 
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]

            string = ""
            for i in temp_array2:
                string += "km:" + i["km"] + ", " +  "open time:" + i["open_time"] + ", "

            return string
        
        elif spec2 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = i["open_time"]
                
            return vals

class listCloseOnly2(Resource):
    def get(self, spec3):
        global times

        top = request.args.get("top")
        if spec3 == "csv":
            
            temp_arr = times
            temp_array2 = sorted(temp_arr, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
                
            string = ""
            for i in temp_array2:
                string += "km:" + i["km"] + ", " +  "close time:" + i["close_time"] + ", "

            return string
        
        elif spec3 == "json":

            temp_array = times
            temp_array2 = sorted(temp_array, key=lambda k: int(k['km']))
        
            if top != None:
                top = int(top)
                temp_array2 = temp_array2[:top]
                
            vals = {}
            for i in temp_array2:
                vals[i["km"]] = i["close_time"]
                
            return vals

# Create routes
# Another way, without decorators
api.add_resource(listAll1, "/listAll")
api.add_resource(listOpenOnly1, "/listOpenOnly")
api.add_resource(listCloseOnly1, "/listCloseOnly")

api.add_resource(listAll2, "/listAll/<spec1>")
api.add_resource(listOpenOnly2, "/listOpenOnly/<spec2>")
api.add_resource(listCloseOnly2, "/listCloseOnly/<spec3>")


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

